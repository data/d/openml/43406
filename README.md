# OpenML dataset: Red-Wine-data-set

https://www.openml.org/d/43406

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Quality Prediction of Red Wine based on Different Feature Sets Using Machine Learning Techniques
Published in International Journal of Science and Research (IJSR) Volume 9 Issue 7, July 2020: Page 6
You can also read the published paper in order to clear concepts 
Link - https://www.ijsr.net/archive/v9i7/v9i75.php 
                                      or
        https://www.ijsr.net/get_abstract.php?paper_id=SR20718002904
Stay safe  Keep Learning

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43406) of an [OpenML dataset](https://www.openml.org/d/43406). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43406/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43406/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43406/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

